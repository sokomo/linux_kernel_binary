#!/usr/bin/env bash
set -e
set -x

ARCH_BUILD_USER="arch_user"
pacman -Syu --noconfirm --needed base-devel bc lzo lzop libelf inetutils
useradd -m ${ARCH_BUILD_USER}
usermod -a -G users ${ARCH_BUILD_USER}
echo "${ARCH_BUILD_USER} ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
sudo -u ${ARCH_BUILD_USER} mkdir -p /home/${ARCH_BUILD_USER}/.gnupg/
sudo -u ${ARCH_BUILD_USER} chmod 700 /home/${ARCH_BUILD_USER}/.gnupg/
sudo -u ${ARCH_BUILD_USER} gpg --keyserver hkp://sks-keyservers.net --recv-keys 79BE3E4300411886 38DBBDC86092693E
sudo -u ${ARCH_BUILD_USER} makepkg -c -s --noconfirm
